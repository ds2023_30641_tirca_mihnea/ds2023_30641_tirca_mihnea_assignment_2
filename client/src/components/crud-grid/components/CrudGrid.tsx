import * as React from 'react';
import Box from '@mui/material/Box';
import EditIcon from '@mui/icons-material/Edit';
import DeleteIcon from '@mui/icons-material/DeleteOutlined';
import SaveIcon from '@mui/icons-material/Save';
import CancelIcon from '@mui/icons-material/Close';
import {
    GridRowsProp,
    GridRowModesModel,
    GridRowModes,
    DataGrid,
    GridColDef,
    GridActionsCellItem,
    GridEventListener,
    GridRowId,
    GridRowModel,
    GridRowEditStopReasons
} from '@mui/x-data-grid';
import {useEffect, useState} from "react";
import EditToolbar from "./EditToolbar";
import {AxiosResponse} from "axios";
import {GridRowUpdateModel} from "../model/GridRowUpdateModel";
import {GridRowCreateModel} from "../model/GridRowCreateModel";
import toast from "react-hot-toast";
import {randomId} from "@mui/x-data-grid-generator";

interface CrudGridInterface {
    rows: GridRowsProp,
    columns: GridColDef[],
    updateFunc: (id: string, updateDto: GridRowUpdateModel) => Promise<AxiosResponse<GridRowModel>>,
    deleteFunc: (id: string) => Promise<AxiosResponse<void>>,
    defaultNewRowFields?: GridRowModel,
    createFunc?: (creationDto: GridRowCreateModel) => Promise<AxiosResponse<GridRowModel>>
}

export default function CrudGrid(props: CrudGridInterface) {
    const [rows, setRows] =
        useState<GridRowsProp>([]);
    const [rowModesModel, setRowModesModel] =
        useState<GridRowModesModel>({});

    useEffect(() => {
        setRows(props.rows);
    }, [props.rows]);

    const defaultNewRow: GridRowModel | undefined = props.defaultNewRowFields ? {
        ...props.defaultNewRowFields,
        id: randomId(),
        isNew: true
    } : undefined;

    const handleRowEditStop: GridEventListener<'rowEditStop'> = (params, event) => {
        if (params.reason === GridRowEditStopReasons.rowFocusOut) {
            event.defaultMuiPrevented = true;
        }
    };

    const handleEditClick = (id: GridRowId) => () => {
        setRowModesModel({...rowModesModel, [id]: {mode: GridRowModes.Edit}});
    };

    const handleSaveClick = (id: GridRowId) => () => {
        setRowModesModel({...rowModesModel, [id]: {mode: GridRowModes.View}});
    };

    const handleDeleteClick = (id: GridRowId) => () => {
        props.deleteFunc(id as string)
            .then(() => {
                setRows((prevRows) => prevRows.filter((row) => row.id !== id));
                toast.success("Record successfully deleted!");
            })
            .catch((error) => {
                console.log(error);
                throw error;
            })
    };

    const handleCancelClick = (id: GridRowId) => () => {
        setRowModesModel({
            ...rowModesModel,
            [id]: {mode: GridRowModes.View, ignoreModifications: true},
        });

        const editedRow = rows.find((row) => row.id === id);
        if (editedRow!.isNew) {
            setRows(rows.filter((row) => row.id !== id));
        }
    };

    const processRowUpdate = (newRow: GridRowModel) => {
        let updatedRow: GridRowModel = {...newRow, isNew: false};
        if(!newRow.isNew) {
            // update

            // exclude id to get update request body
            const updateObj: GridRowUpdateModel = (({ id, ...rest }) => rest)(newRow);

            props.updateFunc(newRow.id as string, updateObj)
                .then((response: AxiosResponse<GridRowModel>) => {
                    updatedRow = {
                        ...updatedRow,
                        ...response.data
                    }
                    setRows(rows.map((row) => (row.id === newRow.id ? updatedRow : row)));
                    toast.success("Record successfully updated!");
                })
                .catch((error: any) => {
                    console.log(error);
                    throw error;
                })
        }
        else if(props.defaultNewRowFields !== undefined && props.createFunc !== undefined) {
            // create

            // exclude id to get create request body
            const createObj: GridRowCreateModel = (({ id, ...rest }) => rest)(newRow);

            props.createFunc(createObj)
                .then((response: AxiosResponse<GridRowModel>) => {
                    updatedRow = {
                        ...updatedRow,
                        ...response.data
                    }
                    console.log(response.data)
                    console.log(updatedRow)

                    // remove new row with temp id and add row from backend
                    setRows((prevRows) => [
                        ...prevRows.filter(row => row.id !== newRow.id),
                        updatedRow
                    ]);

                    toast.success("Record successfully added!");
                })
                .catch((error) => {
                    console.log(error);
                    throw error;
                })
        }
        return updatedRow;
    };

    const handleRowModesModelChange = (newRowModesModel: GridRowModesModel) => {
        setRowModesModel(newRowModesModel);
    };

    const columns: GridColDef[] = [
        ...props.columns,
        {
            field: 'actions',
            type: 'actions',
            headerName: 'Actions',
            width: 100,
            cellClassName: 'actions',
            getActions: ({id}) => {
                const isInEditMode = rowModesModel[id]?.mode === GridRowModes.Edit;

                if (isInEditMode) {
                    return [
                        <GridActionsCellItem
                            icon={<SaveIcon/>}
                            label="Save"
                            onClick={handleSaveClick(id)}
                        />,
                        <GridActionsCellItem
                            icon={<CancelIcon/>}
                            label="Cancel"
                            className="textPrimary"
                            onClick={handleCancelClick(id)}
                        />,
                    ];
                }

                return [
                    <GridActionsCellItem
                        icon={<EditIcon/>}
                        label="Edit"
                        className="textPrimary"
                        onClick={handleEditClick(id)}
                    />,
                    <GridActionsCellItem
                        icon={<DeleteIcon/>}
                        label="Delete"
                        onClick={handleDeleteClick(id)}
                    />,
                ];
            },
        },
    ];

    return (
        <Box
            sx={{
                height: 500,
                width: '100%'
            }}
        >
            <DataGrid
                rows={rows}
                columns={columns}
                editMode="row"
                rowModesModel={rowModesModel}
                onRowModesModelChange={handleRowModesModelChange}
                onRowEditStop={handleRowEditStop}
                processRowUpdate={processRowUpdate}
                onProcessRowUpdateError={(error) => console.log(error)}
                slots={{
                    toolbar: EditToolbar,
                }}
                slotProps={{
                    toolbar: {setRows, setRowModesModel, defaultNewRow},
                }}
            />
        </Box>
    );
}