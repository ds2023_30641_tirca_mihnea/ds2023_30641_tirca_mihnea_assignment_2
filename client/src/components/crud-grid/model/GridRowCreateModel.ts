import {GridRowModel} from "@mui/x-data-grid";

type GridRowCreateModel = Omit<GridRowModel, 'id'>;

export type {GridRowCreateModel}