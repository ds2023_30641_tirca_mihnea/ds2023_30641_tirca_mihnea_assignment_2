import {DeviceStatus} from "./DeviceStatus";

/**
 * Device information displayed on the admin's device list page
 */
export default interface DeviceInfo {
    id: string,
    name: string,
    status: DeviceStatus,
    maxConsumption: number,
    userId: string
}