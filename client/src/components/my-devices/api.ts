import {userManagementAxios} from "../../axios/axios";
import MyDeviceInfo from "./model/MyDeviceInfo";
import {USER_API} from "../../model/constants";

export const getUserDevices = async () => {
    return await userManagementAxios.get<MyDeviceInfo[]>(`${USER_API}/devices`)
}
