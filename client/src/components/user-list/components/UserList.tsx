import {useEffect, useState} from "react";
import {deleteUser, getAllUsers, updateUser} from "../api";
import UserInfo from "../model/UserInfo";
import {GridColDef, GridValidRowModel} from "@mui/x-data-grid";
import CrudGrid from "../../crud-grid/components/CrudGrid";
import {Authorization} from "../../../model/Authorization";
import {GridRowUpdateModel} from "../../crud-grid/model/GridRowUpdateModel";
import {AxiosResponse} from "axios";

const userListColumns: GridColDef[] = [
    {
        field: 'id',
        headerName: 'Id',
        width: 300,
        type: 'string',
        editable: false
    },
    {
        field: 'username',
        headerName: 'Username',
        width: 150,
        type: 'string',
        editable: true
    },
    {
        field: 'firstName',
        headerName: 'First Name',
        width: 150,
        type: 'string',
        editable: true
    },
    {
        field: 'lastName',
        headerName: 'Last Name',
        width: 150,
        type: 'string',
        editable: true
    },
    {
        field: 'role',
        headerName: 'Role',
        width: 150,
        type: 'singleSelect',
        valueOptions: Object.values(Authorization),
        editable: true
    },
]

function UserList() {
    const [users, setUsers] = useState<UserInfo[]>([]);

    useEffect(() => {
        getAllUsers()
            .then(response => {
                setUsers(response.data);
            })
            .catch(error => {
                console.log(error);
                throw error;
            })
    }, []);

    //todo make it work without as unknown as...
    return (
        <>
            <CrudGrid
                rows={users}
                columns={userListColumns}
                updateFunc={(updateUser as unknown) as (id: string, updateDto: GridRowUpdateModel) => Promise<AxiosResponse<GridValidRowModel>>}
                deleteFunc={deleteUser}
            />
        </>
    );
}

export default UserList;