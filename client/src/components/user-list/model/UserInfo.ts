import {Authorization} from "../../../model/Authorization";

/**
 * User information displayed on the admin's user list page
 */
export default interface UserInfo {
    id: string,
    username: string
    firstName: string,
    lastName: string,
    role: Authorization
}