import {Authorization} from "../../../model/Authorization";

/**
 * User dto used in the request body of the 'update user' endpoint
 */
export default interface UserUpdate {
    username: string
    firstName: string,
    lastName: string,
    role: Authorization
}