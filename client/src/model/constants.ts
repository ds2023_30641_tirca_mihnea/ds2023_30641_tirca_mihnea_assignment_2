export const USER_API: string = "/api/users"
export const DEVICE_API: string = "/api/devices"
export const REGISTER_API: string = "/auth/register"
export const AUTHENTICATE_API: string = "/auth/authenticate"