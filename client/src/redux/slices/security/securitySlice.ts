import {createAsyncThunk, createSlice, PayloadAction} from '@reduxjs/toolkit'
import {Authorization} from "../../../model/Authorization";
import {RootState} from "../../store";
import Credentials from "../../../model/Credentials";
import {getUserData, logIn} from "./api";
import UserData from "../../../model/UserData";
import LoginStatus from "../../../model/LoginStatus";
import {userManagementAxios} from "../../../axios/axios";

interface SecurityState {
    userData: UserData | null,
    loginStatus: LoginStatus,
    token: string,
}

const initState = (): SecurityState => {
    return {
        userData: null,
        loginStatus: LoginStatus.LoggedOut,
        token: ""
    };
}
const initialState: SecurityState = initState()

const extractUsername = (token: string): string => {
    const obj = JSON.parse(atob(token.split('.')[1]))
    return obj.sub
}

type AuthenticationData = { token: string, userData: UserData }
export const authenticate = createAsyncThunk(
    "security/login",
    async (logInData: Credentials): Promise<AuthenticationData> => {
        const token: string = await logIn(logInData)
            .catch((error) => {
                throw new Error()
            })
        const userData: UserData = await getUserData(token);
        return {token: token, userData: userData};
    })

export const securitySlice = createSlice({
    name: 'security',
    initialState,
    reducers: {
        logout: (state): void => {
            state.userData = initialState.userData;
            state.loginStatus = initialState.loginStatus;
            state.token = initialState.token;
        }
    },
    extraReducers: (builder): void => {
        builder
            .addCase(authenticate.pending, (state): void => {
                state.loginStatus = LoginStatus.Pending;
            })
            .addCase(authenticate.fulfilled, (state, action: PayloadAction<AuthenticationData>): void => {
                const authenticationData: AuthenticationData = action.payload;
                userManagementAxios.defaults.headers.common['Authorization'] = authenticationData.token;
                state.userData = authenticationData.userData;
                state.loginStatus = LoginStatus.LoggedIn;
                state.token = authenticationData.token;
            })
            .addCase(authenticate.rejected, (state, action): void => {
                state.loginStatus = LoginStatus.LoggedOut;
                console.log(action.error);
                throw action.error;
            })
    }
})

export const {logout} = securitySlice.actions

export const selectAuthorization = (state: RootState): Authorization | null => {
    return state.security.userData ? state.security.userData.role : null;
}
export const selectUserId = (state: RootState): string | null => {
    return state.security.userData ? state.security.userData.id : null;
}
export const selectUsername = (state: RootState): string | null => {
    return state.security.userData ? state.security.userData.username : null;
}
export const selectToken = (state: RootState): string | null => {
    return state.security.token;
}
export const selectIsLoggedIn = (state: RootState): boolean => state.security.loginStatus === LoginStatus.LoggedIn

export default securitySlice.reducer;