import Layout from './components/Layout'
import createSecuredRouter from './createSecuredRouter'
import {Path} from './model/path'
import {Authorization} from "../model/Authorization";
import React from "react";
import MyDevices from "../components/my-devices/components/MyDevices";
import Home from "../components/home/components/Home";
import UserList from "../components/user-list/components/UserList";
import DeviceList from '../components/device-list/components/DeviceList';

const pageRouter = createSecuredRouter([
    {
        path: Path.DEFAULT,
        element: <Layout/>,
        children: [
            {
                path: Path.HOME,
                element: <Home/>
            },
            {
                path: Path.MY_DEVICES,
                element: <MyDevices/>,
                authorization: Authorization.CLIENT
            },
            {
                path: Path.USER_LIST,
                element: <UserList/>,
                authorization: Authorization.ADMIN
            },
            {
                path: Path.DEVICE_LIST,
                element: <DeviceList/>,
                authorization: Authorization.ADMIN
            }
        ]
    }
])

export default pageRouter;