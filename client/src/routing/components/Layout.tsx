import {Outlet} from 'react-router-dom'
import Header from "../../components/header/components/Header";
import {useIsLoggedIn} from "../../redux/slices/security/selectors";
import {Stack} from "@mui/material";

function Layout() {
    return (
        <Stack>
            {useIsLoggedIn() && <Header/>}
            <div style={{marginTop: 80}}>
                <Outlet/>
            </div>
        </Stack>
    )
}

export default Layout;
