import React, {useEffect} from 'react'
import {Path} from '../model/path'
import {NavigateFunction, useNavigate} from "react-router-dom";
import {Authorization} from "../../model/Authorization";
import {useAuthorization, useIsLoggedIn} from "../../redux/slices/security/selectors";

type SecuredPageProps = { page: React.ReactNode; authorization: Authorization | undefined}

const isAllowedAccess = (userAuthorization: Authorization | null, pageAuthorization: Authorization | undefined): boolean => {
    // If page requires no authorization
    if (pageAuthorization === undefined) {
        return true;
    }
    return userAuthorization !== null && userAuthorization === pageAuthorization;
}

const SecuredPage = ({page, authorization}: SecuredPageProps) => {
    const navigate: NavigateFunction = useNavigate();
    const userAuthorization: Authorization | null = useAuthorization();
    const isLoggedIn: boolean = useIsLoggedIn();

    useEffect(() => {
        if (!isLoggedIn) {
            navigate(Path.LOGIN);
        } else if (!isAllowedAccess(userAuthorization, authorization)) {
            navigate(Path.HOME);
        }
    }, [isLoggedIn, userAuthorization, authorization, navigate]);

    return <>{page}</>
}

export default SecuredPage;