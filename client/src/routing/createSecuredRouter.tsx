import SecuredRoute from "./model/SecuredRoute";
import {createBrowserRouter, Navigate, RouteObject} from "react-router-dom";
import SecuredPage from "./components/SecuredPage";
import {Path} from "./model/path";
import Login from "../components/login/components/Login";
import React from "react";

const mapRoutes = (routes: SecuredRoute[]): RouteObject[] => {
    return routes.map(({path, element, authorization, children}): RouteObject => {
        return {
            ...{path: path, element: <SecuredPage page={element} authorization={authorization}/>},
            ...(children && {children: mapRoutes(children)}),
        };
    })
}
const createSecuredRouter = (routes: SecuredRoute[]) => {
    return createBrowserRouter([
        ...mapRoutes(routes),
        {path: Path.LOGIN, element: <Login/>},
        {path: Path.ANY, element: <Navigate to={Path.HOME}/>}
    ]);
}

export default createSecuredRouter;
