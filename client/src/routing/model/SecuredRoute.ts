import React from "react";
import {Path} from "./path";
import {Authorization} from "../../model/Authorization";

type SecuredRoute = {
    path: Path,
    element: React.ReactNode,
    authorization?: Authorization,
    children?: SecuredRoute[]
}
export default SecuredRoute;