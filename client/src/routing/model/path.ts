export enum Path {
    DEFAULT = '/',
    HOME = '/home',
    LOGIN = '/login',
    MY_DEVICES = '/my-devices',
    ANY = '*',
    USER_LIST = "/employees",
    DEVICE_LIST = "/devices",
}