import {createTheme} from "@mui/material";

export const theme = createTheme({
    palette: {
        primary: {
            main: "#609966",
            light: "#9DC08B",
            dark: "#40513B"
        },
        secondary: {
            main: "#9EDDFF",
            light: "#BEFFF7",
            dark: "#6499E9"
        }
    }
})