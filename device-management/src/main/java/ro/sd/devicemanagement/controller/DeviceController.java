package ro.sd.devicemanagement.controller;

import jakarta.validation.Valid;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.web.bind.annotation.*;
import ro.sd.devicemanagement.model.dto.DeviceCreationDto;
import ro.sd.devicemanagement.model.dto.DeviceInfoDto;
import ro.sd.devicemanagement.model.dto.MyDeviceInfoDto;
import ro.sd.devicemanagement.model.dto.DeviceUpdateDto;
import ro.sd.devicemanagement.service.DeviceService;

import java.util.List;
import java.util.UUID;

@RestController
@RequestMapping("/api/devices")
public class DeviceController {
    private final DeviceService deviceService;

    @Autowired
    public DeviceController(DeviceService deviceService) {
        this.deviceService = deviceService;
    }

    @GetMapping("/{id}")
    public MyDeviceInfoDto getById(@PathVariable UUID id) {
        return deviceService.getById(id);
    }

    @ResponseStatus(HttpStatus.CREATED)
    @PostMapping
    public DeviceInfoDto create(@Valid @RequestBody DeviceCreationDto dto) {
        return deviceService.create(dto);
    }

    @DeleteMapping("/{id}")
    public void deleteById(@PathVariable UUID id) {
        deviceService.deleteById(id);
    }

    @GetMapping("/userId/{userId}")
    public List<MyDeviceInfoDto> getMyDevices(@PathVariable UUID userId) {
        return deviceService.getAllDevicesByUserId(userId);
    }

    @PutMapping("/{id}")
    public DeviceInfoDto updateDevice(@PathVariable UUID id, @RequestBody DeviceUpdateDto dto) {
        return deviceService.updateDevice(id, dto);
    }

//TODO @PreAuthorize("hasRole('ROLE_ADMIN')")
    @GetMapping
    public List<DeviceInfoDto> getAllDevices() {
        return deviceService.getAllDevices();
    }
}
