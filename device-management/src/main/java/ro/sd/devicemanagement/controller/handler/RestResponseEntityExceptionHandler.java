package ro.sd.devicemanagement.controller.handler;

import org.springframework.http.ProblemDetail;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.ExceptionHandler;
import org.springframework.web.bind.annotation.RestControllerAdvice;
import org.springframework.web.servlet.mvc.method.annotation.ResponseEntityExceptionHandler;
import ro.sd.devicemanagement.controller.exceptions.EntryNotFoundException;

import static org.springframework.http.ProblemDetail.forStatusAndDetail;

@RestControllerAdvice
public class RestResponseEntityExceptionHandler extends ResponseEntityExceptionHandler {

    @ExceptionHandler(value = {EntryNotFoundException.class})
    public ResponseEntity<ProblemDetail> handleEntryNotFound(EntryNotFoundException e) {
        return ResponseEntity.of(forStatusAndDetail(e.getStatusCode(), e.getMessage())).build();
    }
}
