package ro.sd.devicemanagement.model.dto;

import lombok.Builder;
import ro.sd.devicemanagement.model.entity.Device;
import ro.sd.devicemanagement.model.enums.DeviceChangeType;
import ro.sd.devicemanagement.model.enums.DeviceStatus;

import java.util.UUID;

/**
 * Information sent to RabbitMQ to synchronize the Monitoring and Communication database when a device changes in
 * Device-Management microservice database
 */
@Builder
public record DeviceChangeDto(DeviceChangeType type, UUID id, String name, DeviceStatus status, double maxConsumption, UUID userId) {
    public static DeviceChangeDto createChange(Device device) {
        return new DeviceChangeDto(
                DeviceChangeType.CREATE,
                device.getId(),
                device.getName(),
                device.getStatus(),
                device.getMaxConsumption(),
                device.getUserId()
        );
    }

    public static DeviceChangeDto updateChange(Device device) {
        return new DeviceChangeDto(
                DeviceChangeType.UPDATE,
                device.getId(),
                device.getName(),
                device.getStatus(),
                device.getMaxConsumption(),
                device.getUserId()
        );
    }

    public static DeviceChangeDto deleteChange(Device device) {
        return DeviceChangeDto.builder()
                .type(DeviceChangeType.DELETE)
                .id(device.getId())
                .build();
    }
}
