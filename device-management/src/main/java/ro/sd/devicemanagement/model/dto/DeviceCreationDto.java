package ro.sd.devicemanagement.model.dto;

import ro.sd.devicemanagement.model.entity.Device;
import ro.sd.devicemanagement.model.enums.DeviceStatus;

import java.util.UUID;

/**
 * Information filled by an admin when creating a new device
 */
public record DeviceCreationDto(String name, DeviceStatus status, double maxConsumption, UUID userId) {
    public static Device toEntity(DeviceCreationDto dto) {
        return Device.builder()
                .name(dto.name())
                .status(dto.status())
                .maxConsumption(dto.maxConsumption())
                .userId(dto.userId())
                .build();
    }
}
