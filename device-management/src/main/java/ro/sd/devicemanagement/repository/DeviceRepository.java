package ro.sd.devicemanagement.repository;

import org.springframework.data.repository.CrudRepository;
import org.springframework.stereotype.Repository;
import ro.sd.devicemanagement.model.entity.Device;

import java.util.List;
import java.util.UUID;

@Repository
public interface DeviceRepository extends CrudRepository<Device, UUID> {
    List<Device> findAllByUserId(UUID userId);
}
