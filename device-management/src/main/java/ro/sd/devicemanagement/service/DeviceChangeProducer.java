package ro.sd.devicemanagement.service;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.amqp.rabbit.core.RabbitTemplate;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.stereotype.Service;
import ro.sd.devicemanagement.model.dto.DeviceChangeDto;

@Service
public class DeviceChangeProducer {

    @Value("${rabbitmq.exchange}")
    private String exchange;

    @Value("${rabbitmq.routing_key}")
    private String routingKey;

    private final RabbitTemplate rabbitTemplate;

    private static final Logger LOGGER = LoggerFactory.getLogger(DeviceChangeProducer.class);

    @Autowired
    public DeviceChangeProducer(RabbitTemplate rabbitTemplate) {
        this.rabbitTemplate = rabbitTemplate;
    }

    public void produceDeviceChange(DeviceChangeDto dto) {
        rabbitTemplate.convertAndSend(exchange, routingKey, dto);
        LOGGER.info("Message sent: Change device " + dto);
    }
}
