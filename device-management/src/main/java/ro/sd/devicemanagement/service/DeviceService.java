package ro.sd.devicemanagement.service;

import jakarta.transaction.Transactional;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import ro.sd.devicemanagement.controller.exceptions.EntryNotFoundException;
import ro.sd.devicemanagement.model.dto.*;
import ro.sd.devicemanagement.model.entity.Device;
import ro.sd.devicemanagement.model.enums.DeviceChangeType;
import ro.sd.devicemanagement.repository.DeviceRepository;

import java.util.List;
import java.util.UUID;

@Service
@Transactional
public class DeviceService {
    private final DeviceRepository deviceRepository;

    private final DeviceChangeProducer producer;

    @Autowired
    public DeviceService(DeviceRepository deviceRepository, DeviceChangeProducer producer) {
        this.deviceRepository = deviceRepository;
        this.producer = producer;
    }

    public MyDeviceInfoDto getById(UUID id) {
        Device device = deviceRepository.findById(id)
                .orElseThrow(() -> new EntryNotFoundException(Device.class, id));

        return MyDeviceInfoDto.fromEntity(device);
    }

    public DeviceInfoDto create(DeviceCreationDto creationDto) {
        Device device = DeviceCreationDto.toEntity(creationDto);
        Device savedDevice = deviceRepository.save(device);

        producer.produceDeviceChange(DeviceChangeDto.createChange(device));

        return DeviceInfoDto.fromEntity(savedDevice);
    }

    public void deleteById(UUID id) {
        Device device = deviceRepository.findById(id)
                .orElseThrow(() -> new EntryNotFoundException(Device.class, id));

        deviceRepository.deleteById(id);
        producer.produceDeviceChange(DeviceChangeDto.deleteChange(device));
    }

    public List<MyDeviceInfoDto> getAllDevicesByUserId(UUID userId) {
        List<Device> devices = deviceRepository.findAllByUserId(userId);

        return devices.stream()
                .map(MyDeviceInfoDto::fromEntity)
                .toList();
    }

    public List<DeviceInfoDto> getAllDevices() {
        List<Device> devices = (List<Device>) deviceRepository.findAll();

        return devices.stream()
                .map(DeviceInfoDto::fromEntity)
                .toList();
    }

    public DeviceInfoDto updateDevice(UUID id, DeviceUpdateDto dto) {
        Device device = deviceRepository.findById(id)
                .orElseThrow(() -> new EntryNotFoundException(Device.class, id));

        dto.update(device);
        Device savedDevice = deviceRepository.save(device);

        producer.produceDeviceChange(DeviceChangeDto.updateChange(device));

        return DeviceInfoDto.fromEntity(savedDevice);
    }
}
