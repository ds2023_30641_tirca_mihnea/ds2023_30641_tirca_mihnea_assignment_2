package ro.sd.meteringsimulator;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.scheduling.annotation.EnableScheduling;

@SpringBootApplication
@EnableScheduling
public class MeteringSimulatorApplication {

    public static void main(String[] args) {
        SpringApplication.run(MeteringSimulatorApplication.class, args);
    }

}
