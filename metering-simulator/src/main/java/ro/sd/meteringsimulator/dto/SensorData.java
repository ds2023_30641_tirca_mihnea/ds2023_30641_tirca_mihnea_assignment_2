package ro.sd.meteringsimulator.dto;

import java.util.UUID;

public record SensorData(long timestamp, UUID device_id, double measurement_value) {
}
