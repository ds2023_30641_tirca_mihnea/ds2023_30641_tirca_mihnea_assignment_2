package ro.sd.moncom;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.scheduling.annotation.EnableScheduling;

@SpringBootApplication
@EnableScheduling
public class MonComApplication {

	public static void main(String[] args) {
		SpringApplication.run(MonComApplication.class, args);
	}

}
