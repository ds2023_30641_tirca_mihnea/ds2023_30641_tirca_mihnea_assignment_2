package ro.sd.moncom.dto;

import ro.sd.moncom.model.entity.Device;
import ro.sd.moncom.model.enums.DeviceChangeType;
import ro.sd.moncom.model.enums.DeviceStatus;

import java.util.UUID;

public record DeviceChangeDto(DeviceChangeType type, UUID id, String name, DeviceStatus status, double maxConsumption, UUID userId) {
    public Device toEntity() {
        return new Device(id, name, status, 0, maxConsumption, userId);
    }
}
