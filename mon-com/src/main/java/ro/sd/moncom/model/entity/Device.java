package ro.sd.moncom.model.entity;

import jakarta.persistence.*;
import lombok.*;
import ro.sd.moncom.model.enums.DeviceStatus;

import java.util.UUID;

@Entity
@Getter
@Setter
@Builder
@NoArgsConstructor
@AllArgsConstructor
public class Device {
    @Id
    private UUID id;

    private String name;

    @Enumerated(EnumType.STRING)
    private DeviceStatus status;

    private double hourlyConsumption;

    private double maxConsumption;

    private UUID userId;
}
