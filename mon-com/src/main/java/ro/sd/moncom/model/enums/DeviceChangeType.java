package ro.sd.moncom.model.enums;

public enum DeviceChangeType {
    CREATE,
    UPDATE,
    DELETE
}
