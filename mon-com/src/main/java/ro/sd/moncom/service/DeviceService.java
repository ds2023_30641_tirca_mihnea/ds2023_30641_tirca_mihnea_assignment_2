package ro.sd.moncom.service;

import jakarta.persistence.EntityNotFoundException;
import jakarta.transaction.Transactional;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.scheduling.annotation.Scheduled;
import org.springframework.stereotype.Service;
import ro.sd.moncom.dto.DeviceChangeDto;
import ro.sd.moncom.dto.SensorData;
import ro.sd.moncom.model.entity.Device;
import ro.sd.moncom.repository.DeviceRepository;

import java.util.List;
import java.util.UUID;

@Service
public class DeviceService {
    private final DeviceRepository deviceRepository;
    private final Notifier notifier;

    private boolean reachedMaxConsumption;

    @Autowired
    public DeviceService(DeviceRepository deviceRepository, Notifier notifier) {
        this.deviceRepository = deviceRepository;
        this.notifier = notifier;
        this.reachedMaxConsumption = false;
    }

    @Transactional
    public void saveSensorData(SensorData sensorData) {
        Device device = deviceRepository.findById(sensorData.device_id())
                .orElseThrow(EntityNotFoundException::new);

        device.setHourlyConsumption(device.getHourlyConsumption() + sensorData.measurement_value());
        deviceRepository.save(device);

        if (!reachedMaxConsumption && device.getHourlyConsumption() > device.getMaxConsumption()) {
            notifier.notifyUser(device);
            reachedMaxConsumption = true;
        }
    }

    // Doesn't check consumption per hour, but per 10 seconds
    @Scheduled(fixedDelay = 1000 * 10)
    void clearConsumption() {
        List<Device> devices = (List<Device>) deviceRepository.findAll();
        devices.forEach(device -> device.setHourlyConsumption(0.0));
        deviceRepository.saveAll(devices);
        reachedMaxConsumption = false;
    }

    @Transactional
    public void update(DeviceChangeDto changeDto) {
        Device device = deviceRepository.findById(changeDto.id())
                .orElseThrow(EntityNotFoundException::new);

        device.setId(changeDto.id());
        device.setName(changeDto.name());
        device.setStatus(changeDto.status());
        device.setMaxConsumption(changeDto.maxConsumption());
        device.setUserId(changeDto.userId());

        deviceRepository.save(device);
    }

    @Transactional
    public void delete(UUID id) {
        deviceRepository.deleteById(id);
    }

    @Transactional
    public void create(DeviceChangeDto changeDto) {
        Device device = changeDto.toEntity();
        deviceRepository.save(device);
    }

    @Transactional
    public void change(DeviceChangeDto changeDto) {
        switch (changeDto.type()) {
            case CREATE -> create(changeDto);
            case UPDATE -> update(changeDto);
            case DELETE -> delete(changeDto.id());
        }
    }
}

