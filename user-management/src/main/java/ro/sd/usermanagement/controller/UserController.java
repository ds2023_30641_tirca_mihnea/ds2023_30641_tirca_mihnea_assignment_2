package ro.sd.usermanagement.controller;

import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.security.access.prepost.PreAuthorize;
import org.springframework.web.bind.annotation.*;
import ro.sd.usermanagement.model.dto.*;
import ro.sd.usermanagement.service.UserService;

import java.util.List;
import java.util.UUID;

@RestController
@RequestMapping("/api/users")
@Slf4j
public class UserController {
    private final UserService userService;

    @Autowired
    public UserController(UserService userService) {
        this.userService = userService;
    }

    @PreAuthorize("hasRole('ROLE_ADMIN')")
    @GetMapping("/{id}")
    public UserInfoDto getById(@PathVariable UUID id) {
        return userService.getById(id);
    }

    @PreAuthorize("hasRole('ROLE_ADMIN')")
    @GetMapping("/username/{username}")
    public UserInfoDto getByUsername(@PathVariable String username) {
        return userService.getByUsername(username);
    }

    @PreAuthorize("hasRole('ROLE_ADMIN')")
    @DeleteMapping("/{id}")
    public void deleteById(@PathVariable UUID id) {
        userService.deleteById(id);
    }

    @PreAuthorize("hasRole('ROLE_ADMIN')")
    @PutMapping("/{id}")
    public UserInfoDto updateUser(@PathVariable UUID id, @RequestBody UserUpdateDto dto) {
        return userService.updateUser(id, dto);
    }

    @PreAuthorize("hasAnyRole('ROLE_CLIENT', 'ROLE_ADMIN')")
    @GetMapping("/data")
    public UserDataDto getUserData(@RequestHeader(name = "Authorization") String token) {
        return userService.getUserData(token);
    }

    @PreAuthorize("hasRole('ROLE_CLIENT')")
    @GetMapping("/devices")
    public List<MyDeviceInfoDto> getUserDevices(@RequestHeader(name = "Authorization") String token) {
        return userService.getUserDevices(token);
    }

    @PreAuthorize("hasRole('ROLE_ADMIN')")
    @GetMapping
    public List<UserInfoDto> getAllUsers() {
        return userService.getAllUsers();
    }
}
