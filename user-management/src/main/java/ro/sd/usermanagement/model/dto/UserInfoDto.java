package ro.sd.usermanagement.model.dto;

import lombok.Builder;
import ro.sd.usermanagement.model.entity.User;
import ro.sd.usermanagement.model.enums.UserRole;

import java.util.UUID;

/**
 * User information displayed on the admin's user list page
 */
@Builder
public record UserInfoDto(UUID id, String username, String firstName, String lastName, UserRole role) {
    public static UserInfoDto fromEntity(User user) {
        return UserInfoDto.builder()
                .id(user.getId())
                .username(user.getUsername())
                .firstName(user.getFirstName())
                .lastName(user.getLastName())
                .role(user.getRole())
                .build();
    }
}
