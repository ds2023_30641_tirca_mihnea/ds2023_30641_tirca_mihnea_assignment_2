package ro.sd.usermanagement.model.enums;

public enum DeviceStatus {
    ONLINE,
    OFFLINE
}
