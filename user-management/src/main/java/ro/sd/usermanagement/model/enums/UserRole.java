package ro.sd.usermanagement.model.enums;

public enum UserRole {
    ADMIN,
    CLIENT
}
