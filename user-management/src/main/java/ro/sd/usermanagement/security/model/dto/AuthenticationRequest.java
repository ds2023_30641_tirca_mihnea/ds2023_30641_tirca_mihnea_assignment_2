package ro.sd.usermanagement.security.model.dto;

public record AuthenticationRequest(String username, String password) {
}
