package ro.sd.usermanagement.security.model.dto;

public record AuthenticationResponse(String access_token) {
}
