package ro.sd.usermanagement.service;

import jakarta.transaction.Transactional;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import ro.sd.usermanagement.controller.exceptions.EntryNotFoundException;
import ro.sd.usermanagement.controller.exceptions.UserNotFoundException;
import ro.sd.usermanagement.model.dto.*;
import ro.sd.usermanagement.model.entity.User;
import ro.sd.usermanagement.repository.UserRepository;
import ro.sd.usermanagement.security.service.JwtService;

import java.util.List;
import java.util.UUID;

@Service
@Transactional
public class UserService {
    private final UserRepository userRepository;

    private final JwtService jwtService;

    private final RestService restService;

    @Autowired
    public UserService(UserRepository userRepository, JwtService jwtService, RestService restService) {
        this.userRepository = userRepository;
        this.jwtService = jwtService;
        this.restService = restService;
    }

    public UserInfoDto getById(UUID id) {
        User user = userRepository.findById(id)
                .orElseThrow(() -> new EntryNotFoundException(User.class, id));

        return UserInfoDto.fromEntity(user);
    }

    public void deleteById(UUID id) {
        userRepository.findById(id)
                .orElseThrow(() -> new EntryNotFoundException(User.class, id));

        userRepository.deleteById(id);
    }

    public UserInfoDto updateUser(UUID id, UserUpdateDto dto) {
        User user = userRepository.findById(id)
                .orElseThrow(() -> new EntryNotFoundException(User.class, id));

        dto.update(user);

        User savedUser = userRepository.save(user);

        return UserInfoDto.fromEntity(savedUser);
    }

    public UserInfoDto getByUsername(String username) {
        User user = userRepository.findByUsername(username)
                .orElseThrow(() -> new UserNotFoundException(username));

        return UserInfoDto.fromEntity(user);
    }

    public UserDataDto getUserData(String bearerToken) {
        String username = jwtService.extractUsernameFromBearerToken(bearerToken);

        User user = userRepository.findByUsername(username)
                .orElseThrow(() -> new UserNotFoundException(username));

        return UserDataDto.fromEntity(user);
    }

    public List<MyDeviceInfoDto> getUserDevices(String bearerToken) {
        String username = jwtService.extractUsernameFromBearerToken(bearerToken);
        User user = userRepository.findByUsername(username)
                .orElseThrow(() -> new UserNotFoundException(username));
        UUID userId = user.getId();

        return restService.getAllDevicesByUserId(userId);
    }

    public List<UserInfoDto> getAllUsers() {
        List<User> users = (List<User>) userRepository.findAll();

        return users.stream()
                .map(UserInfoDto::fromEntity)
                .toList();

    }

}
